import { LitElement, html } from "lit-element";
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../data-manager-persona-main/data-manager-persona-main.js';

class PersonaMain extends LitElement {

    static get properties() {
      return {
        people: {type: Array},
        showPersonForm: {type: Boolean}
      };
    }

    constructor() {
      super();

      /* sin DM:
      this.people = [
        {
          name: "Ellen Ripley",
          yearsInCompany: 10,
          photo: {
            src: "./img/persona.jpg",
            alt: "Ellen Ripley"
          },
          profile: "Lorem ipsum dolor sit amet."
        }, {
          name: "Bruce Banner",
          yearsInCompany: 2,
          photo: {
            src: "./img/persona.jpg",
            alt: "Bruce Banner"
          },
          profile: "Lorem ipsum."
        }, {
          name: "Éowyn",
          yearsInCompany: 5,
          photo: {
            src: "./img/persona.jpg",
            alt: "Éowyn"
          },
          profile: "Lorem."
        }, {
          name: "Lmz",
          yearsInCompany: 4,
          photo: {
            src: "./img/persona.jpg",
            alt: "Éowyn"
          },
          profile: "sit amet."
        }, {
          name: "Eit",
          yearsInCompany: 3,
          photo: {
            src: "./img/persona.jpg",
            alt: "Éowyn"
          },
          profile: "dolor sit amet."
        }
      ]
      */
      this.people = [];

      this.showPersonForm = false;
    }

    render () {
        return html`
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
          <h2 class="text-center">Personas</h2>
          <div class="row" id="peopleList">
            <div class="row row-cols-1 row-cols-sm-4">
            ${this.people.map(
              person => html`<persona-ficha-listado
               fname="${person.name}"
               yearsInCompany="${person.yearsInCompany}"
               profile="${person.profile}"
               .photo="${person.photo}"
               @delete-person="${this.deletePerson}"
               @info-person="${this.infoPerson}"
              ></persona-ficha-listado>`
            )}
            </div>
          </div>
          <div class="row">
            <persona-form
              @persona-form-close="${this.personFormClose}"
              @persona-form-store="${this.personFormStore}"
              class="d-none border rounded border-primary"
              id="personForm"></persona-form>
          </div>
          <data-manager-persona-main
          @people-data-updated="${this.peopleDataUpdated}"
          ></data-manager-persona-main>
        `;
    }

    updated(changedProperties) {
      console.log("updated");

      if (changedProperties.has("showPersonForm")) {
        console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

        if (this.showPersonForm === true) {
          this.showPersonFormData();
        } else {
          this.showPersonList();
        }
      }

      if (changedProperties.has("people")) {
        console.log("Ha cambiado el valor de la propiedad people en persona-main");

        this.dispatchEvent(
          new CustomEvent(
            "updated-people",
            {
              detail : {
                people : this.people
              }
            }
          )
        )
      }
    }

    peopleDataUpdated(e) {
      console.log("peopleDataUpdated");

      this.people = e.detail.people;
    }

    infoPerson(e){
      console.log("infoPerson");
      console.log("Se ha pedido más información de la persona " + e.detail.name);

      let chosenPerson = this.people.filter(
        person => person.name === e.detail.name
      );

      //console.log(chosenPerson);
      //console.log(chosenPerson[0].name);

      let person = {};
      person.name = chosenPerson[0].name;
      person.profile = chosenPerson[0].profile;
      person.yearsInCompany = chosenPerson[0].yearsInCompany;

      this.shadowRoot.getElementById("personForm").person = person;
      this.shadowRoot.getElementById("personForm").editingPerson = true;
      this.showPersonForm = true;
    }

    personFormClose() {
      console.log("personFormClose");
      console.log("Se ha cerrado el formulario de la persona");

      this.showPersonForm = false;
    }

    personFormStore(e) {
      console.log("personFormStore");
      console.log("Se va a almacenar una persona");
      console.log(e.detail.person);

      if (e.detail.editingPerson === true) {
        console.log("Se va a actualizar la persona de nombre " +
          e.detail.person.name);

        /*
        let indexOfPerson = this.people.findIndex(
          person => person.name === e.detail.person.name
        );

        if (indexOfPerson >= 0) {
          console.log("Persona encontrada");

          this.people[indexOfPerson] = e.detail.person;
      }*/

        this.people = this.people.map(
          person => person.name === e.detail.person.name
            ? person = e.detail.person : person)
      } else {
          console.log("Se va a almacenar una persona nueva");

          this.people = [...this.people, e.detail.person];

          /*this.people.push(e.detail.person);*/

      }

      console.log("Proceso terminado");

      this.showPersonForm = false;
    }

    showPersonFormData() {
      console.log("showPersonFormData");
      console.log("Mostrando formulario de persona");

      this.shadowRoot.getElementById("personForm").classList.remove("d-none");
      this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    showPersonList() {
      console.log("showPersonList");
      console.log("Mostrando listado de personas");

      this.shadowRoot.getElementById("personForm").classList.add("d-none");
      this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    deletePerson(e) {
      console.log("deletePerson en persona-main");
      console.log("se va a borrar la persona de nombre" + e.detail.name);

      this.people = this.people.filter(
        person => person.name != e.detail.name
      );
    }
}

customElements.define('persona-main', PersonaMain);
