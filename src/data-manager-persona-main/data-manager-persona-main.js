import { LitElement, html } from "lit-element";

class DataManagerPersonaMain extends LitElement {

    static get properties() {
      return {
        people: {type: Array}
      }
    }

    constructor() {
      super();

      this.people = [
        {
          name: "Ellen Ripley",
          yearsInCompany: 10,
          photo: {
            src: "./img/persona.jpg",
            alt: "Ellen Ripley"
          },
          profile: "Lorem ipsum dolor sit amet."
        }, {
          name: "Bruce Banner",
          yearsInCompany: 2,
          photo: {
            src: "./img/persona.jpg",
            alt: "Bruce Banner"
          },
          profile: "Lorem ipsum."
        }, {
          name: "Éowyn",
          yearsInCompany: 5,
          photo: {
            src: "./img/persona.jpg",
            alt: "Éowyn"
          },
          profile: "Lorem."
        }, {
          name: "Lmz",
          yearsInCompany: 4,
          photo: {
            src: "./img/persona.jpg",
            alt: "Éowyn"
          },
          profile: "sit amet."
        }, {
          name: "Eit",
          yearsInCompany: 3,
          photo: {
            src: "./img/persona.jpg",
            alt: "Éowyn"
          },
          profile: "dolor sit amet."
        }
      ]

    }

    updated(changedProperties) {
      console.log("updated");

      if (changedProperties.has("people")) {
        console.log("Ha cambiado el valor de la propiedad people");

        this.dispatchEvent(
          new CustomEvent(
            "people-data-updated",
             {
                detail : {
                  people: this.people
                }
              }
          )
        )
      }
    }
}

customElements.define('data-manager-persona-main', DataManagerPersonaMain);
