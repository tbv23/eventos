import { LitElement, html, css } from "lit-element";

class TestBootStrap extends LitElement {

  static get styles() {
    return css`
      .redbg  {
        background-color: red;
      }

      .greenbg  {
        background-color: green;
      }

      .bluebg  {
        background-color: blue;
      }

      .greybg  {
        background-color: grey;
      }
    `;
  }

  constructor() {
    super();
  }

    render () {
        return html`

            <h3>Test Bootstrap</h3>
            <div class="row greybg">
              <div class="col redbg">Col 1</div>
              <div class="col greenbg">Col 2</div>
              <div class="col bluebg">Col 3</div>
            </div>
        `;
    }
}

customElements.define('test-bootstrap', TestBootStrap);
